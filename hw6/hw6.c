#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <math.h>
#include <string.h>
#include <locale.h>

#define MAX_WIDTH_HEIGHT 28000
//#define MAX_WIDTH_HEIGHT 2048
//#define NUMTASKS 8 
#define NUMTASKS MAX_WIDTH_HEIGHT
#define CENTER_X -1.186340599860225
#define CENTER_Y -0.303652988644423
#define MAX_ITERATIONS 100
#define HUE_PER_ITERATION 5
#define ROWS_PER_JOB 100

// Comms tags
#define WORK_REQUEST_TAG 0
#define WORK_COMPLETE_TAG 1
#define ALL_COMPLETE_TAG 2

typedef struct {
  int numProcessors;
  int rank;
  double *xVals;
} World;

double When();
World * getWorld();
void freeWorld(World *world);
void runServer(World *world);
void runWorker(World *world);
double *generateCoords(int rank, int start, int end);
void workJob(unsigned char (*results)[MAX_WIDTH_HEIGHT][3], int startRow, int endRow, World *world);
double iterationsToEscape(double x, double y, int maxIterations);
int hue2rgb(double t);
void writeImage(unsigned char *img, int w, int h);

World * getWorld() {
  World *world = malloc(sizeof(World));

  MPI_Comm_size(MPI_COMM_WORLD, &world->numProcessors);
  MPI_Comm_rank(MPI_COMM_WORLD, &world->rank);

  world->xVals = generateCoords(world->rank, 0, MAX_WIDTH_HEIGHT);

  return world;
}

void freeWorld(World *world) {
  free(world->xVals);

  free(world);
}

int main(int argc, char *argv[]) {
  World *world;

  setlocale(LC_NUMERIC, "");

  MPI_Init(&argc, &argv);
  world = getWorld();

  if (world->rank == 0) {
    runServer(world);
  } else {
    runWorker(world);
  }

  MPI_Finalize();

  freeWorld(world);

  return 0;
}

void runServer(World *world) {
  int i;
  int nextJob = 0;
  int jobsMap[world->numProcessors][2];
  unsigned char receiveCompletedWorkBuffer[ROWS_PER_JOB][MAX_WIDTH_HEIGHT][3];
  MPI_Status status;
  int countCompleted = 1; // We don't wait for ourselves to complete
  int placeHolder;
  long long size = (long long)NUMTASKS * (long long)MAX_WIDTH_HEIGHT * 3;
  //unsigned char img[size];
  unsigned char *img;
  long long offset;
  size_t copySize = sizeof(unsigned char) * ROWS_PER_JOB * 3 * MAX_WIDTH_HEIGHT;
  double startTime = When();

  img = (unsigned char*)malloc(size * sizeof(unsigned char)); 
  printf("(%d) Server size: %lld copySize: %zu\n", world->rank, size, copySize);

  while (countCompleted < world->numProcessors) {
    MPI_Recv(receiveCompletedWorkBuffer, (ROWS_PER_JOB * MAX_WIDTH_HEIGHT * 3), MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    if (status.MPI_TAG == WORK_COMPLETE_TAG) {
      int rangeStart;
      int rangeEnd;

      rangeStart = jobsMap[status.MPI_SOURCE][0];
      rangeEnd = jobsMap[status.MPI_SOURCE][1];

      offset = (long long)rangeStart * MAX_WIDTH_HEIGHT * 3;

      //printf("(%d) rangeStart: %d rangeEnd: %d copies at offset: %'llu, with copySize: %zu\n", world->rank, rangeStart, rangeEnd, offset, copySize);

      memcpy(img+offset, &receiveCompletedWorkBuffer[0], copySize);

      //printf("(%d) %d finished task %d-%d\n", world->rank, status.MPI_SOURCE, rangeStart, rangeEnd);
      //printf("%d\n", receiveCompletedWorkBuffer[0][0][0]);
    }

    if (nextJob < NUMTASKS) {
      jobsMap[status.MPI_SOURCE][0] = nextJob;
      jobsMap[status.MPI_SOURCE][1] = nextJob + ROWS_PER_JOB;
      nextJob = jobsMap[status.MPI_SOURCE][1];
      //printf("(%d) Sending %d job %d-%d\n", world->rank, status.MPI_SOURCE, jobsMap[status.MPI_SOURCE][0], jobsMap[status.MPI_SOURCE][1]);
      MPI_Send(jobsMap[status.MPI_SOURCE], 2, MPI_INT, status.MPI_SOURCE, WORK_REQUEST_TAG, MPI_COMM_WORLD);
    } else {
      countCompleted++;
      MPI_Send(jobsMap[status.MPI_SOURCE], 1, MPI_INT, status.MPI_SOURCE, ALL_COMPLETE_TAG, MPI_COMM_WORLD);
    }
  }

  printf("%d, 0, %f\n", world->rank, When() - startTime);

  printf("(%d) Going to write the image now\n", world->rank);
  writeImage(img, MAX_WIDTH_HEIGHT, NUMTASKS);
  free(img);

  //printf("(%d) Server done\n", world->rank);
}

void runWorker(World *world) {
  int keepGoing = 1;
  unsigned char workCompleteBuffer[ROWS_PER_JOB][MAX_WIDTH_HEIGHT][3];
  int jobRange[2];
  MPI_Status status;

  //printf("(%d) Worker\n", world->rank);

  MPI_Send(workCompleteBuffer, 1, MPI_UNSIGNED_CHAR, 0, WORK_REQUEST_TAG, MPI_COMM_WORLD);
  MPI_Recv(jobRange, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  if (status.MPI_TAG == ALL_COMPLETE_TAG) keepGoing = 0;

  while (keepGoing) {
    //printf("(%d) I got told to do task %d-%d\n", world->rank, jobRange[0], jobRange[1]);
    workJob(workCompleteBuffer, jobRange[0], jobRange[1], world);

    MPI_Send(workCompleteBuffer, ROWS_PER_JOB * MAX_WIDTH_HEIGHT * 3, MPI_UNSIGNED_CHAR, 0, WORK_COMPLETE_TAG, MPI_COMM_WORLD);  
    MPI_Recv(jobRange, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    if (status.MPI_TAG == ALL_COMPLETE_TAG) keepGoing = 0;
  }
}

void workJob(unsigned char (*results)[MAX_WIDTH_HEIGHT][3], int startRow, int endRow, World *world) {
  int x;
  int y;
  double translatedY;
  double iterations;
  unsigned char r;
  unsigned char g;
  unsigned char b;
  double hue;

  //printf("(%d) Running %d-%d\n", world->rank, startRow, endRow);

  for (y = startRow; y < endRow; y++) {
//    printf("(%d) y = %d\n", world->rank, y);
    translatedY = (y - NUMTASKS / 2) + CENTER_Y;

    for (x = 0; x < MAX_WIDTH_HEIGHT; ++x) {
//      printf("(%d) x = %d\n", world->rank, x);
      iterations = iterationsToEscape(world->xVals[x], translatedY, MAX_ITERATIONS);
//      printf("(%d) iterations: %f\n", world->rank, iterations);

      if (iterations != -1) {
        hue = HUE_PER_ITERATION * iterations;
        r = hue2rgb(hue + 120);
        g = hue2rgb(hue);
        b = hue2rgb(hue + 240);

        results[y-startRow][x][2] = r;
        results[y-startRow][x][1] = g;
        results[y-startRow][x][0] = b;
//        printf("(%d) rgb: %u %u %u\n", world->rank, (unsigned int)results[y][x][0], (unsigned int)results[y][x][1], (unsigned int)results[y][x][2]);
      }
    }
  }
}

double *generateCoords(int rank, int start, int end) {
  int i;
  double *coords = malloc(sizeof(double) * (end - start));

  //printf("(%d) generateCoords: start: %d, end: %d\n", rank, start, end);

  for (i = start; i < end; ++i) {
    coords[i] = (i - MAX_WIDTH_HEIGHT/2) + CENTER_X;
  }

  return coords;
}

double iterationsToEscape(double x, double y, int maxIterations) {
    double tempa;
    double a = 0;
    double b = 0;
    int i;

    for (i = 0 ; i < maxIterations ; i++) {
        tempa = a*a - b*b + x;
        b = 2*a*b + y;
        a = tempa;
        if (a*a+b*b > 64) {
            // return i; // discrete
            return i - log(sqrt(a*a+b*b))/log(8); //continuous
        }
    }
    return -1;
}

int hue2rgb(double t){
    while (t>360) {
        t -= 360;
    }
    if (t < 60) return 255.*t/60.;
    if (t < 180) return 255;
    if (t < 240) return 255. * (4. - t/60.);
    return 0;
}

void writeImage(unsigned char *img, int w, int h) {
    long long filesize = 54 + 3*(long long)w*(long long)h;
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};
    FILE *f;
    long long offset;
    int i;
    unsigned char *unassigned;

    printf("Writing with %d x %d\n", w, h);

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);

    f = fopen("temp.bmp","wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    for (i=0; i<h; i++) {
        offset = ((long long)w*(h-i-1)*3);
        fwrite(img+offset,3,w,f);
        fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }
    fclose(f);
}

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
