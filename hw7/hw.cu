#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define BLOCKSIZE 1024
#define MAXIT 400
#define TOTROWS		(BLOCKSIZE*8)
#define TOTCOLS		(BLOCKSIZE*8)
#define NOTSETLOC       -1 // for cells that are not fixed
#define SETLOC       1 // for cells that are fixed
#define EPSILON 0.1

#define QMAX(x,y) (((x) > (y))? (x): (y))


int *lkeepgoing;
float *iplate;
float *oplate;
int *fixed;
float *tmp;
int ncols, nrows;

double When();
void Compute();


int main(int argc, char *argv[])
{
	double t0, tottime;
	ncols = TOTCOLS;
	nrows = TOTROWS;

	fprintf(stderr, "======================\n");
	cudaMalloc((void **) &lkeepgoing, nrows * ncols * sizeof(int));
	cudaMalloc((void **) &iplate, nrows * ncols * sizeof(float));
	cudaMalloc((void **) &oplate, nrows * ncols * sizeof(float));
	cudaMalloc((void **) &fixed,  nrows * ncols * sizeof(float));
	fprintf(stderr,"Memory allocated\n");

	t0 = When();
	/* Now proceed with the Jacobi algorithm */
	Compute();

	tottime = When() - t0;
	fprintf(stderr, "Total Time is: %lf sec.\n", tottime);
	fprintf(stderr, "======================\n");

	return 0;
}

__global__ void InitArrays(float *ip, float *op, int *fp, int *kp, int ncols)
{
	int i;
	float *oppos, *ippos;
	int *fppos;
  int *kppos;
  int blockOffset;
  int rowStartPos;
  int colsPerThread;
	
  // Each block gets a row, each thread will fill part of a row

 // printf("%d blockIdx.x, %d thread id, %d gridDim.x, %d blockDim.x \n", blockIdx.x, threadIdx.x, gridDim.x, blockDim.x);
	// Calculate the offset of the row
  blockOffset = blockIdx.x * ncols;
  // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
  // The number of cols per thread
  colsPerThread = ncols/blockDim.x;

	ippos = ip + blockOffset+ rowStartPos;
	fppos = fp + blockOffset+ rowStartPos;
	oppos = op + blockOffset+ rowStartPos;
	kppos = kp + blockOffset+ rowStartPos;

  // This sets everything to 50
	for (i = 0; i < colsPerThread; i++) {
	  if (blockIdx.x == 0) { // Set the top
		  fppos[i] = SETLOC; // Fixed
		  ippos[i] = 0.0f;
		  oppos[i] = 0;
	    kppos[i] = 0;
	  } else if (blockIdx.x == gridDim.x - 1) { // set the bottom
		  fppos[i] = SETLOC; // Fixed
		  ippos[i] = 100;
		  oppos[i] = 100;
	    kppos[i] = 0;
	  } else if (blockIdx.x == 400 && threadIdx.x == 0 && i <= 330) {
		  fppos[i] = SETLOC; // Fixed
		  ippos[i] = 100;
		  oppos[i] = 100;
	    kppos[i] = 0;
	  } else {
		  fppos[i] = NOTSETLOC; // Not Fixed
		  ippos[i] = 50;
		  oppos[i] = 50;
	    kppos[i] = 1; // Keep Going
	  }
	}

	// Set the left-most border to 0
	if (threadIdx.x == 0) {
    fppos[0] = SETLOC; // Fixed
    ippos[0] = 0;
    oppos[0] = 0;
    kppos[0] = 0;
	}

	// Set the right-most border to 0
	if (threadIdx.x == blockDim.x - 1) {
    fppos[colsPerThread-1] = SETLOC; // Fixed
    ippos[colsPerThread-1] = 0;
    oppos[colsPerThread-1] = 0;
    kppos[colsPerThread-1] = 0;
	}

  // Arbitrarily pick some other thread to do this one
	if (blockIdx.x == 1 && threadIdx.x == 1) {
    fp[200 * ncols + 500] = SETLOC;
    ip[200 * ncols + 500] = 100;
    op[200 * ncols + 500] = 100;
    kppos[200 * ncols + 500] = 0;
	}
}
__global__ void doCalc(float *iplate, float *oplate, int *fplate, int ncols)
{
	/* Compute the 5 point stencil for my region */
	int i;
  int blockOffset;
  int rowStartPos;
  int startPosition;
  int endPosition;
  int colsPerThread;
	
  // Each block gets a row, each thread will fill part of a row

  // My start offset is:
  //  My row * the width + 
	// Calculate the offset of the row
  blockOffset = blockIdx.x * ncols;
  // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
  // The number of cols per thread
  colsPerThread = ncols/blockDim.x;

  startPosition = blockOffset + rowStartPos;
  endPosition = startPosition + colsPerThread;

  // I want to start the loop at my lowest offset, and then end it at my max offset
  // I prefer this, because it makes the math for my neighbors easier to compute.
	for (i = startPosition; i < endPosition; i++) {
	  if (fplate[i] == NOTSETLOC) {
	    oplate[i] = (
	      iplate[i - ncols] +
	      iplate[i + ncols] +
	      iplate[i - 1] +
	      iplate[i + 1] +
	      (4 * iplate[i])) * 0.125f;
	  }
	}
}

__global__ void doCheck(float *iplate, float *oplate, int *fixed, int *lkeepgoing, int ncols)
{
	// Calculate keepgoing array
	int i;
  int blockOffset;
  int rowStartPos;
  int startPosition;
  int endPosition;
  int colsPerThread;
  float epsilon;
	
  // Each block gets a row, each thread will fill part of a row

  // My start offset is:
  //  My row * the width + 
	// Calculate the offset of the row
  blockOffset = blockIdx.x * ncols;
  // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
  // The number of cols per thread
  colsPerThread = ncols/blockDim.x;

  startPosition = blockOffset + rowStartPos;
  endPosition = startPosition + colsPerThread;

  // I want to start the loop at my lowest offset, and then end it at my max offset
  // I prefer this, because it makes the math for my neighbors easier to compute.
	for (i = startPosition; i < endPosition; i++) {
	  if (fixed[i] == NOTSETLOC) {
	    epsilon = fabs(
	      ( ( oplate[i - ncols] + oplate[i + ncols] + oplate[i - 1] + oplate[i + 1] ) * 0.25f) -
	        oplate[i]
	      );
	      
	      if (epsilon > EPSILON) {
	        lkeepgoing[i] = 1;
	      } else {
          lkeepgoing[i] = 0;
	      }

	      //if (i == TOTCOLS + 1) {
        //  printf("cur: %f, epsilon: %f, keepgoing:%d\n", oplate[i], epsilon, lkeepgoing[i]);
	      //}
	  } else {
      lkeepgoing[i] = 0;
	  }
	}
}

__global__ void fakeReduction(int *keepgoing) {
  *keepgoing = 0;
}

__global__ void doCheckCheat(float *iplate, float *oplate, int *fixed, int *lkeepgoing, int ncols)
{
	// Calculate keepgoing array
	int i;
  int blockOffset;
  int rowStartPos;
  int startPosition;
  int endPosition;
  int colsPerThread;
  float epsilon;

  // Each block gets a row, each thread will fill part of a row

  // My start offset is:
  //  My row * the width + 
	// Calculate the offset of the row
  blockOffset = blockIdx.x * ncols;
  // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
  // The number of cols per thread
  colsPerThread = ncols/blockDim.x;

  startPosition = blockOffset + rowStartPos;
  endPosition = startPosition + colsPerThread;

  // I want to start the loop at my lowest offset, and then end it at my max offset
  // I prefer this, because it makes the math for my neighbors easier to compute.
	for (i = startPosition; i < endPosition; i++) {
	  if (fixed[i] == NOTSETLOC) {
	    epsilon = fabs(
	      ( ( oplate[i - ncols] + oplate[i + ncols] + oplate[i - 1] + oplate[i + 1] ) * 0.25f) -
	        oplate[i]
	      );
	      
	      if (epsilon > EPSILON) {
	        *lkeepgoing = 1;
	      }

	      //if (i == TOTCOLS + 1) {
        //  printf("cur: %f, epsilon: %f, keepgoing:%d\n", oplate[i], epsilon, lkeepgoing[i]);
	      //}
	  }
	}
}

__global__ void reduceSingle(int *idata, int *single, int nrows)
{
	// Reduce rows to the first element in each row
	int i;
	extern __shared__ int parts[];
	
	// Sum my part of one dimensional array and put it shared memory
	parts[threadIdx.x] = 0;
	for (i = threadIdx.x; i < nrows; i+=blockDim.x) {
		parts[threadIdx.x] += idata[i];
	}
	int tid = threadIdx.x;
  if (tid < 512) { parts[tid] += parts[tid + 512];}  
  __syncthreads();
  if (tid < 256) { parts[tid] += parts[tid + 256];}
  __syncthreads();
  if (tid < 128) { parts[tid] += parts[tid + 128];}
  __syncthreads();
  if (tid < 64) { parts[tid] += parts[tid + 64];}
  __syncthreads();
  if (tid < 32) { parts[tid] += parts[tid + 32];}
  __syncthreads();

	if(threadIdx.x == 0) {
		*single = 0;
		for(i = 0; i < 32; i++) {
			*single += parts[i];
		}
	}
}

__global__ void reduceSingleInterleavedAddressing(int *idata, int *single, int nrows)
{
	extern __shared__ int parts[];
	
	unsigned int tid = threadIdx.x;
	unsigned int i;
	unsigned int colsPerThread = nrows / blockDim.x;
	unsigned int threadStartPos = tid * colsPerThread;
	
	parts[tid] = idata[threadStartPos];
	for (i = 1; i < colsPerThread; ++i) {
  	parts[tid] += idata[threadStartPos + i];
  }
  __syncthreads();

  for (i = 1; i < blockDim.x; i *= 2) {
    if (tid % (2 * i) == 0) {
      parts[tid] += parts[tid + i];
    }
    __syncthreads();
  }

  if (tid == 0) {
    *single = parts[0];
  }
}

__global__ void reduceSingleInterleavedAddressingNonDivergent(int *idata, int *single, int nrows)
{
	extern __shared__ int parts[];
	
	unsigned int tid = threadIdx.x;
	unsigned int i;
	unsigned int colsPerThread = nrows / blockDim.x;
	unsigned int threadStartPos = tid * colsPerThread;
	
	parts[tid] = idata[threadStartPos];
	for (i = 1; i < colsPerThread; ++i) {
  	parts[tid] += idata[threadStartPos + i];
  }
  __syncthreads();

  for (i = 1; i < blockDim.x; i *= 2) {
    int index = 2 * i * tid;

    if (index < blockDim.x) {
      parts[index] += parts[index + i];
    }
    __syncthreads();
  }

  if (tid == 0) {
    *single = parts[0];
  }
}

__global__ void reduceSingleSequential(int *idata, int *single, int nrows)
{
	extern __shared__ int parts[];
	
	unsigned int tid = threadIdx.x;
	unsigned int i;
	unsigned int colsPerThread = nrows / blockDim.x;
	unsigned int threadStartPos = tid * colsPerThread;
	
	parts[tid] = idata[threadStartPos];
	for (i = 1; i < colsPerThread; ++i) {
  	parts[tid] += idata[threadStartPos + i];
  }
  __syncthreads();

  for (i = blockDim.x/2; i > 0; i >>= 1) {
    if (tid < i) {
      parts[tid] += parts[tid + i];
    }

    __syncthreads();
  }

  if (tid == 0) {
    *single = parts[0];
  }
}

__global__ void reduceSum(int *idata, int *odata, unsigned int ncols)
{
	// Reduce rows to the first element in each row
	int i;
  int blockOffset;
  int rowStartPos;
  int colsPerThread;
  int *mypart;
	
  // Each block gets a row, each thread will reduce part of a row

	// Calculate the offset of the row
  blockOffset = blockIdx.x * ncols;
  // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
  // The number of cols per thread
  colsPerThread = ncols/blockDim.x;

	mypart = idata + blockOffset + rowStartPos;

	// Sum all of the elements in my thread block and put them 
        // into the first column spot
	for (i = 1; i < colsPerThread; i++) {
		mypart[0] += mypart[i];
	}
	__syncthreads(); // Wait for everyone to complete
        // Now reduce all of the threads in my block into the first spot for my row
	if(threadIdx.x == 0) {
		odata[blockIdx.x] = 0;
		for(i = 0; i < blockDim.x; i++) {
			odata[blockIdx.x] += mypart[i*colsPerThread];
		}
	}
	// We cant synchronize between blocks, so we will have to start another kernel
}

__global__ void reduceSumCheat(int *idata, int *keepGoing, unsigned int ncols)
{
	// Reduce rows to the first element in each row
	int i;
  int blockOffset;
  int rowStartPos;
  int colsPerThread;
  int *mypart;
	
  // Each block gets a row, each thread will reduce part of a row

	// Calculate the offset of the row
  blockOffset = blockIdx.x * ncols;
  // Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
  // The number of cols per thread
  colsPerThread = ncols/blockDim.x;

	mypart = idata + blockOffset + rowStartPos;

	// Sum all of the elements in my thread block and put them 
        // into the first column spot
	for (i = 0; i < colsPerThread; i++) {
	  if (mypart[i] > 0) {
	    *keepGoing = 1;
	  }
	}
}
	
void Compute()
{
	int *keepgoing_single;
	int *keepgoing_sums;
	int keepgoing;
	int blocksize = BLOCKSIZE;
	int iteration;
 // int timeit;
//	double t0, tottime;

	ncols = TOTCOLS;
	nrows = TOTROWS;

	// One block per row
	InitArrays<<< nrows, blocksize >>>(iplate, oplate, fixed, lkeepgoing, ncols);

	cudaMalloc((void **)&keepgoing_single, 1 * sizeof(int));
	keepgoing = 1;
	cudaMalloc((void **)&keepgoing_sums, nrows * sizeof(int));
 	//int *peek = (int *)malloc(nrows*sizeof(int));

	for (iteration = 0; (iteration < MAXIT) && keepgoing; iteration++)
	{
		doCalc<<< nrows, blocksize >>>(iplate, oplate, fixed, ncols);
		//doCheck<<< nrows, blocksize >>>(iplate, oplate, fixed, lkeepgoing, ncols);
	  fakeReduction<<<1, 1>>>(keepgoing_single);	
		doCheckCheat<<< nrows, blocksize >>>(iplate, oplate, fixed, keepgoing_single, ncols);


		//reduceSum<<< nrows, blocksize>>>(lkeepgoing, keepgoing_sums, ncols);
		//reduceSumCheat<<< nrows, blocksize>>>(lkeepgoing, keepgoing_single, ncols);
		// Now we have the sum for each row in the first column, 
		//  reduce to one value
		//t0 = When();
		//for(timeit = 0; timeit < 1000000; timeit++){
			//reduceSingle<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
			//reduceSingleInterleavedAddressing<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
			//reduceSingleInterleavedAddressingNonDivergent<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
		//	reduceSingleSequential<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
		//}
		//tottime = When()-t0;
		
		keepgoing = 0;
		//cudaMemcpy(&keepgoing, keepgoing_single, 1 * sizeof(int), cudaMemcpyDeviceToHost);
		cudaMemcpy(&keepgoing, keepgoing_single, 1 * sizeof(int), cudaMemcpyDeviceToHost);
//		fprintf(stderr, "keepgoing = %d time %f\n", keepgoing, tottime);
		//fprintf(stderr, "keepgoing = %d \n", keepgoing);

		/* swap the new value pointer with the old value pointer */
		tmp = oplate;
		oplate = iplate;
		iplate = tmp;
	}
	//free(peek);
	cudaFree(keepgoing_single);
	cudaFree(keepgoing_sums);
	fprintf(stderr,"Finished in %d iterations\n", iteration);
}

/* Return the current time in seconds, using a double precision number.       */
double When()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

