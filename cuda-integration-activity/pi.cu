#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <cuda.h>


#define STEPS      2000
#define PI25DT      3.141592653598793238462643

__global__ void pi_calc(double *a, int N, int blockSize, int intervals) {
  int  i;
  double sum;
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  int start;
  int end;

  if (idx < N) {
    sum=0.0;
    start = (idx * blockSize) + 1;
    end = ((idx + 1) * blockSize) + 1;
    printf("idx: %d, going from %d to %d\n", idx, start, end);
    for (i=start; i<end; i++){ 
      double x=((double) i-0.5)/ ((double)intervals);
      sum=sum+(4.0/(1.0+x*x));
    }
    a[idx] = sum;
    printf("idx: %d, sum: %f\n", idx, a[idx]);
  }
}

__global__ void idOnDevice(double *a, int N)
{
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  if (idx<N) {
    printf("ids: %d, blockIdx %d, blockDim %d, ThreadIdx %d\n",idx, blockIdx.x,blockDim.x,threadIdx.x);
    a[idx] = 3;
  }
}

int main(void){
  int i;
  clock_t cpu_start_time;
  clock_t cpu_end_time;
  //int intervals = 100000;
  int intervals = 1000;
  double *resultsOnHost;
  double *resultsOnDevice;
  int blockSize = 100;
  int numberOfBlocks = intervals / blockSize;
  size_t size = numberOfBlocks * sizeof(double);
  double sum;

  resultsOnHost = (double *)malloc(size);
  cudaMalloc((void **) &resultsOnDevice, size);

  cpu_start_time = clock();
  fprintf(stderr, "numberOfBlocks: %d, blockSize: %d\n", numberOfBlocks, blockSize);
//  pi_calc <<< numberOfBlocks, blockSize >>> (resultsOnDevice, numberOfBlocks, blockSize, intervals);
  idOnDevice <<< numberOfBlocks, blockSize >>> (resultsOnDevice, numberOfBlocks);
  cudaMemcpy(resultsOnHost, resultsOnDevice, size, cudaMemcpyDeviceToHost);

  for (i = 0; i < numberOfBlocks; ++i) {
    printf("%d\n", resultsOnHost[i]);
  }
  cpu_end_time  = clock();

  clock_t cpu_time = cpu_end_time - cpu_start_time;

  double cpu_time_double = (double)cpu_time;

  printf("cpu_time: %f\n", cpu_time_double);
  
  return 0;
}

// 1 divide up the work
// communicate those boundaries to the GPUs
// Gpu runs its stuff
