#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

//int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
//                          void *(*start_routine) (void *), void *arg);

#define MAXTHREADS 24

void *threadFunc(void *arg) {
  long myId = (long)arg;
  printf("Hello from thread %lu!\n", myId);
}

int main(int argc, char *argv[]) {
  int numThreads;
  pthread_t threads[MAXTHREADS];
  long i;

  numThreads = atoi(argv[1]);

  for (i=0; i < numThreads; ++i) {
    pthread_create(&threads[i], NULL, &threadFunc, (void *)i);
  }

  for (i=0; i < numThreads; ++i) {
    pthread_join(threads[i], NULL);
  }

  printf("All done from master!\n");

  return 0;
}
