#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


main(int argc, char *argv[]) 
{
  int iproc, nproc,i, iSendTo, rankReceived, iReceiveFrom;
  char host[255], message[55];
  MPI_Status status;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &iproc);

  gethostname(host,253);
  printf("I am proc %d of %d running on %s\n", iproc, nproc,host);

  iSendTo = (iproc + 1) % nproc;

  printf("(%d) Sending message to %d\n", iproc, iSendTo);
  sprintf(message, "%d: Hello\n", iproc);
  MPI_Send(&iproc, 1, MPI_INT, iSendTo, 0, MPI_COMM_WORLD);
  //MPI_Send(message, 35, MPI_CHAR, iSendTo, 0, MPI_COMM_WORLD);

  iReceiveFrom = iproc - 1;
  if (iReceiveFrom < 0) iReceiveFrom = nproc - 1;
  
  printf("(%d) Receiving message from %d\n", iproc, iReceiveFrom);
  MPI_Recv(&rankReceived, 1, MPI_INT, iReceiveFrom, 0, MPI_COMM_WORLD, &status);
  //MPI_Recv(message, 35, MPI_CHAR, iReceiveFrom, 0, MPI_COMM_WORLD, &status);
//  printf("%d: I received %d\n", iproc, rankReceived);
  printf("%d: %s",iproc, message);

  MPI_Finalize();
}
