#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdbool.h>
#include <math.h>

#define SIZE   16384 
//#define SIZE  8192 
#define EPSILON  0.1

double When();
void printGrid();
void printRow(int row);

// The grids and such
bool **needsUpdateMap;
float **t;
float **tMinusOne;
float **tmp;
int startRow, endRow;
int rank, numNodes;
int chunkSize; // how many actual rows a processor is responsible for
int rowCount;  // how many rows it allocates (it's 2 more than chunkSize)
bool iAmEven;
MPI_Status status;


void performTick() {
  int row, col;

  for (row = 1; row < rowCount - 1; ++row) {
    for (col = 1; col < SIZE - 1; ++col) {
      if (needsUpdateMap[row][col]) {

        //fprintf(stderr, "(%d) %f, %f, %f, %f, me: %f\n", rank, tMinusOne[row-1][col], tMinusOne[row+1][col], tMinusOne[row][col-1], tMinusOne[row][col+1], tMinusOne[row][col]);
        // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8
        t[row][col] = ( 
                        tMinusOne[row-1][col] +     // above
                        tMinusOne[row+1][col] +     // below
                        tMinusOne[row][col-1] +     // left
                        tMinusOne[row][col+1] +     // right
                        (4 * tMinusOne[row][col])   // this one
                      ) * 0.125f;
      }
    }
  }
}

void doComms() {
  int send, receive, partner;

  if (numNodes == 1) return;

  // Transmit new top rows
  if (iAmEven) {
    send = chunkSize;
    receive = chunkSize + 1;
    partner = rank + 1;

    //fprintf(stderr, "(%d) Sending (%d) to %d\n", rank, send, partner);
    //printRow(send);

    MPI_Send(t[send], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD);
    MPI_Recv(t[receive], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD, &status);

    //fprintf(stderr, "(%d) Received (%d) from %d\n", rank, receive, partner);
    //printRow(receive);
  } else {
    send = 1;
    receive = 0;
    partner = rank - 1;

    MPI_Recv(t[receive], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD, &status);

    //fprintf(stderr, "(%d) Received (%d) from %d\n", rank, receive, partner);
    //printRow(receive);
    //fprintf(stderr, "(%d) Sending (%d) to %d\n", rank, send, partner);
    //printRow(send);

    MPI_Send(t[send], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD);
  }

  // Transmit new bottom rows
  if (iAmEven) {
    if (rank > 0) {
      send = 1;
      receive = 0;
      partner = rank - 1;

      //fprintf(stderr, "(%d) Sending (%d) to %d\n", rank, send, partner);
      //printRow(send);

      MPI_Send(t[send], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD);
      MPI_Recv(t[receive], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD, &status);

      //fprintf(stderr, "(%d) Received (%d) from %d\n", rank, receive, partner);
      //printRow(receive);
    }
  } else {
    if (rank < numNodes - 1) {
      send = chunkSize;
      receive = chunkSize + 1;
      partner = rank + 1;

      MPI_Recv(t[chunkSize + 1], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD, &status);

      //fprintf(stderr, "(%d) Received (%d) from %d\n", rank, receive, partner);
      //printRow(chunkSize+1);
      //fprintf(stderr, "(%d) Sending (%d) to %d\n", rank, send, partner);
      //printRow(chunkSize);

      MPI_Send(t[chunkSize], SIZE, MPI_FLOAT, partner, 0, MPI_COMM_WORLD);
    }
  }
}

bool weNeedAnUpdate() {
  int row, col;

  for (row = 1; row < rowCount - 1; ++row) {
    for (col = 1; col < SIZE - 1; ++col) {
      //fprintf(stderr, "(%d) Checking for update at %d, %d\n", rank, row, col);
      if (needsUpdateMap[row][col]) {

        if (fabs( 
                 ( ( t[row-1][col] + t[row+1][col] + t[row][col-1] + t[row][col+1] ) * 0.25f )  - 
                 t[row][col] 
               ) > EPSILON) {
          return true;
        }
      }
    }
  }

  return false;
}

void allocateArrays() {
  int row, col;
  int realRow;

  t = (float **)malloc(sizeof(float *) * rowCount);
  tMinusOne = (float **)malloc(sizeof(float *) * rowCount);
  needsUpdateMap = (bool **)malloc(sizeof(bool *) * rowCount);

  for (row = 0; row < rowCount; ++row) { // allocate all of them, because initial state is known for all squares, and we need the neighbor's rows to perform our ticks
    t[row] = (float *)malloc(sizeof(float) * SIZE);
    tMinusOne[row] = (float *)malloc(sizeof(float) * SIZE);
    needsUpdateMap[row] = (bool *)malloc(sizeof(bool) * SIZE);

    realRow = (rank * chunkSize) + row - 1;

      for (col = 0; col < SIZE; ++col) {
        //fprintf(stderr, "allocating %d, %d\n", row, col);
        if (col == 0 || col == SIZE - 1) {
          t[row][col] = tMinusOne[row][col] = 0.0f;
          needsUpdateMap[row][col] = false;
        } else if (realRow == 0) {
          t[row][col] = tMinusOne[row][col] = 0.0f;
          needsUpdateMap[row][col] = false;
        } else if (realRow == SIZE - 1) {
          t[row][col] = tMinusOne[row][col] = 100.0f;
          needsUpdateMap[row][col] = false;
        } else {
          t[row][col] = tMinusOne[row][col] = 50.0f;
          needsUpdateMap[row][col] = true;
        }
      }
    }
}

void main(int argc, char *argv[])
{
    int cnt = 0;
    bool allDone = false;
    bool everyoneDone;

    double starttime;

    MPI_Init(&argc, &argv);
    starttime = When();

    MPI_Comm_size(MPI_COMM_WORLD, &numNodes);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    chunkSize = SIZE / numNodes;
    rowCount = chunkSize + 2;
    iAmEven = rank % 2 == 0;

    //fprintf(stderr,"%d: Hello from %d of %d with chunk size %d\n", rank, rank, numNodes, chunkSize);

    allocateArrays();

    //if (rank == 3) printGrid();

    do {
      performTick();
      allDone = !weNeedAnUpdate();
      doComms();
      everyoneDone = true;
      MPI_Allreduce(&allDone, &everyoneDone, 1, MPI_C_BOOL, MPI_LAND, MPI_COMM_WORLD);
      tmp = t;
      t = tMinusOne;
      tMinusOne = t;
      ++cnt;
    } while (!everyoneDone);

    /* print out the number of iterations to relax */
    fprintf(stderr, "%d:It took %d iterations and %lf seconds to relax the system\n", rank,cnt,When() - starttime);
//    printGrid();
    MPI_Finalize();
}
    
/* Return the correct time in seconds, using a double precision number.       */
double
When()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

void printGrid() {
  int row, col;

  if (SIZE > 16) return;

  for (row = 0; row < rowCount ; ++row) {
    fprintf(stderr, "(%d):", rank);

    for (col = 0; col < SIZE; ++col) {
      fprintf(stderr, "%f ", t[row][col]);
    }

    fprintf(stderr, "\n");
  }
}

void printRow(int row) {
  int col;


  fprintf(stderr, "(%d): ", rank);
  for (col = 0; col < SIZE; ++col) {
    fprintf(stderr, "%f ", rank, t[row][col]);
  }
  fprintf(stderr, "\n");
}
