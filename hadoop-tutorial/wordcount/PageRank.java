import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

public class PageRank extends MapReduceBase implements 
  Mapper<LongWritable, Text, Text, Text>,
  Reducer<Text, Text, Text, Text> {

  private static final float damping = 0.85f;
  
  @Override
  public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
    int pageTabIndex = value.find("\t");
    int rankTabIndex = value.find("\t", pageTabIndex+1);
    
    String page = Text.decode(value.getBytes(), 0, pageTabIndex);
    String pageWithRank = Text.decode(value.getBytes(), 0, rankTabIndex+1);
    
    output.collect(new Text(page), new Text("!"));
    
    //Skip pages with no links
    if(rankTabIndex == -1) return;
    
    String links = Text.decode(value.getBytes(), rankTabIndex+1, value.getLength()-(rankTabIndex+1));
    String[] allOtherPages = links.split(",");
    int totalLinks = allOtherPages.length;
    
    for(String otherPage : allOtherPages) {
      Text pageRankTotalLinks = new Text(pageWithRank + totalLinks);
      output.collect(new Text(otherPage), pageRankTotalLinks);
    }
    
    // Put the original links of the page for the reducer output
    output.collect(new Text(page), new Text("|" + links));
  }
  
  @Override
  public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
    boolean isExistingWikiPage = false;
    String[] split;
    float sumShareOtherPageRanks = 0;
    String links = "";
    String pageWithRank;
    
    while(values.hasNext()) {
      pageWithRank = values.next().toString();
      
      if(pageWithRank.equals("!")) {
        isExistingWikiPage = true;
        continue;
      }
      
      if(pageWithRank.startsWith("|")) {
        links = "\t"+pageWithRank.substring(1);
        continue;
      }
      
      split = pageWithRank.split("\\t");
      
      float pageRank = Float.valueOf(split[1]);
      int countOutLinks = Integer.valueOf(split[2]);
      
      sumShareOtherPageRanks += (pageRank/countOutLinks);
    }
    
    if(!isExistingWikiPage) return;
    float newRank = damping * sumShareOtherPageRanks + (1-damping);
    
    output.collect(key, new Text(newRank + links));
  }

  public static void main(String[] args) throws Exception {
    JobConf conf = new JobConf(PageRank.class);
    
    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);
    
    conf.setInputFormat(TextInputFormat.class);
    conf.setOutputFormat(TextOutputFormat.class);
    
    FileInputFormat.addInputPath(conf, new Path(args[0]));
    FileOutputFormat.setOutputPath(conf, new Path(args[1]));
    
    conf.setMapperClass(PageRank.class);
    conf.setReducerClass(PageRank.class);
    
    JobClient.runJob(conf);
  }
}
