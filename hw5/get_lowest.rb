base = ENV['BASE_FILE']

[2,4,8,16].each do |size|
  file = "#{base}_#{size}.times"

  read_lines = 0
  total_times = []
  compare_times = []
  exchange_times = []
  lowest_total = 9999
  lowest_compare = 9999
  lowest_exchange = 9999

  File.open(file).each do |line|
    next if line.start_with? '/bin'

    split_line = line.split(',')
    if split_line[2].to_f < lowest_total
      lowest_total = split_line[2].to_f
      lowest_compare = split_line[3].to_f
      lowest_exchange = split_line[4].to_f
    end

    read_lines += 1

    if read_lines == size
      total_times << lowest_total
      compare_times << lowest_compare
      exchange_times << lowest_exchange

      read_lines = 0
      lowest_total = 9999
      lowest_compare = 9999
      lowest_exchange = 9999
    end
  end
  puts "#{size}, #{total_times.reduce(:+) / 10}, #{compare_times.reduce(:+) / 10}, #{exchange_times.reduce(:+) / 10}"
  total_times = []
  compare_times = []
  exchange_time = []
end
