 #!/bin/bash

for numProcs in 2 4 8 16
do
  SIZES_FILE="$BASE_FILE$numProcs.sizes"
  TIMES_FILE="$BASE_FILE$numProcs.times"

  rm -rf $SIZES_FILE $TIMES_FILE

  for i in `seq 1 10`;
  do
          NP=$numProcs make run 2>> $SIZES_FILE 1>> $TIMES_FILE
  done   
done

