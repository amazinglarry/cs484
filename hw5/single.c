#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>

#define SIZE 10000000 // If you don't set this to a power of 2, you are a scoundrel
#define MAX_VALUE 1000

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int ascending(const void *a, const void *b) {
  return *(int *)a - *(int *)b;
}

int list[SIZE];

int main() {
  int i;
  double start = When();

  for (i = 0; i < SIZE; ++i) {
    list[i] = rand() % MAX_VALUE;
  }

  qsort(list, SIZE, sizeof(int), ascending);

  printf("done in %f\n", When() - start);
}
