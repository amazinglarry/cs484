#include <stdio.h>
#include <sys/time.h>

#define SIZE 10000000

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  int i = 0;
  int res;
  double start = When();
  
  for (i; i < SIZE; ++i) {
    res = 3 < 4; 
  }

  printf("%f\n", (When() - start) );

  return 0;
}
