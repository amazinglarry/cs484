base = ENV['BASE_FILE']

[2,4,8,16].each do |size|
  file = "#{base}_#{size}.sizes"

  read_lines = 0
  sizes = []
  dimensions = Math.log2(size).to_i
  mins = []
  maxes = []
  differences = []

  File.open(file).each do |line|
    line.strip!

    split_line = line.split(',')[2...(dimensions+2)]

    if split_line
      (0...dimensions).each do |dimension|
        if mins[dimension]
          mins[dimension] = [mins[dimension], split_line[dimension].to_i].min
          maxes[dimension] = [maxes[dimension], split_line[dimension].to_i].max
        else
          mins[dimension] = split_line[dimension].to_i
          maxes[dimension] = split_line[dimension].to_i
        end
      end

      read_lines += 1
      if (read_lines == size)
        read_lines = 0
        mins.each_with_index do |min, i|
          differences[i] ||= 0
          differences[i] += maxes[i] - min
        end
      end
    end
  end
  puts "#{size}, #{differences.map{|d| d / 10 / size}.join(', ')}"
end
