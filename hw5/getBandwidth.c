#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>

#define ITERATIONS 10
#define SIZE 1000000

double When();

int main(int argc, char *argv[]) {
  char host[255];
  MPI_Status status;
  double startTime;
  int rank;
  int numProcessors;
  int send[SIZE];
  int i;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  gethostname(host, 253);

  startTime = When();

  for (i = 0; i < ITERATIONS; ++i) {
    if (rank == 0) {
      MPI_Send(&send, SIZE, MPI_INT, 1, 0, MPI_COMM_WORLD);
    } else {
      MPI_Recv(&send, SIZE, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
    }
  }

  printf("%d, %f\n", rank, (When() - startTime) / ITERATIONS);


  MPI_Finalize();

  return 0;
}

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
