#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <math.h>

int numProcessors;
int worldRank;
int numDimensions;

int main(int argc, char *argv[]) {
  char host[255];
  MPI_Status status;
  int i;
  int isOdd;
  MPI_Comm comm1;
  MPI_Comm comm2;
  int comVal;
  int pivot1, pivot2;
  int newRank1, newRank2;

  gethostname(host, 253);

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
  MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);

  //printf("(%d) of %d, running on %s, %d dimensions\n", worldRank, numProcessors, host, numDimensions);

  isOdd = worldRank % 2;

  MPI_Comm_split(MPI_COMM_WORLD, isOdd, worldRank, &comm1);
  MPI_Comm_rank(comm1, &newRank1);

  pivot1 = worldRank;

  MPI_Bcast(&pivot1, 1, MPI_INT, 0, comm1);

//comVal  printf("(World rank %d), on comm1, our root had world rank %d\n", worldRank, comVal);
//  printf("rank %d, newrank1 %d, pivot1 %d\n", worldRank, newRank1, pivot1);

  isOdd = newRank1 % 2;

  MPI_Comm_split(comm1, isOdd, newRank1, &comm2);
  MPI_Comm_rank(comm2, &newRank2);

  pivot2 = worldRank;
  MPI_Bcast(&pivot2, 1, MPI_INT, 0, comm2);

  printf(
    "rank %d, newrank1 %d, pivot1 %d newrank2 %d pivot2 %d\n"
  , worldRank
  , newRank1
  , pivot1
  , newRank2
  , pivot2
  );


  MPI_Finalize();

  return 0;
}
