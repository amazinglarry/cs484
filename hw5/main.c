#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <math.h>
#include <string.h>

// TODO: 10 million total
// TODO: Timing starts AFTER generating the list

#define SIZE 10000000
//#define SIZE 100000 
//#define SIZE 16384 
#define MAX_DIMENSIONS 5
#define MAX_VALUE 1000
#define MAX_PROCESSORS 32

typedef struct {
  MPI_Comm communicator;
  int rank;
  int pivot;
  int communicatorSize;
} Communicator;

double When();
void buildCommunicators(int toDimension);
void genList();
void printList(int *myList, int count, int line);
int randomValueInList(int *list, int size);
void step(int dimension, void(*medianStrategy)(Communicator *));
int getMedian(int *arr, int size);
void pivotFromMedianOfRootList(Communicator *comm);
void pivotFromMedianOfMedians(Communicator *comm);
void pivotFromRandomElementInRootList(Communicator *comm);
void randomPivot(Communicator *comm);
int ascending(const void *a, const void *b);
int descending(const void *a, const void *b);

Communicator communicators[MAX_DIMENSIONS];
int numProcessors;
int numDimensions;

int size; // how big my list is
int rank;
int list[SIZE];
int buffer[SIZE];
int medians[MAX_PROCESSORS];
int sizeAtEachStep[MAX_DIMENSIONS];
double compareTime; // Time spent getting which pivot to use
double exchangeTime; // Time spent communicating new pivot

int msg;

int main(int argc, char *argv[]) {
  char host[255];
  MPI_Status status;
  int i;
  double startTime = When();

  msg = 0;
  compareTime = 0;
  exchangeTime = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  srand(time(NULL) + rank);
  numDimensions = log2(numProcessors);
  size = SIZE / numProcessors;

  gethostname(host, 253);

  //printf("%d: of %d, running on %s, %d dimensions\n", rank, numProcessors, host, numDimensions);

  genList();

  // Set up the global communicator
  communicators[0].communicator = MPI_COMM_WORLD;
  communicators[0].rank = rank;
  communicators[0].communicatorSize = numProcessors;

  buildCommunicators(1);

  //printList(list, size, msg++);
  //step(0, pivotFromRandomElementInRootList);
  //step(0, pivotFromMedianOfMedians);
  //step(0, pivotFromMedianOfRootList);
  step(0, randomPivot);

  qsort(list, size, sizeof(int), ascending);

  printf("%d, 0, %f, %f, %f\n", rank, When() - startTime, compareTime, exchangeTime);
  printList(sizeAtEachStep, numDimensions, ++msg);

//  printList(list, size, msg++);

  MPI_Finalize();

  return 0;
}

int ascending(const void *a, const void *b) {
  return *(int *)a - *(int *)b;
}

int descending(const void *a, const void *b) {
  return *(int *)b - *(int *)a;
}

void step(int dimension, void(*pivotStrategy)(Communicator *)) {
  Communicator *currentCommunicator;
  int sendFrom;
  int currentlyEven;
  int sendTo;
  MPI_Status status;
  int countReceived;
  double startTime; // used for timing

  if (dimension == numDimensions) return;
  
  currentCommunicator = &communicators[dimension];
  currentlyEven = currentCommunicator->rank % 2 == 0;

  startTime = When();
  qsort(list, size, sizeof(int), currentlyEven ? ascending : descending);
  compareTime += When() - startTime;

  pivotStrategy(currentCommunicator);

  startTime = When();
  //printf("%d: The pivot that I got was %d\n", rank, currentCommunicator->pivot);
  // send all the numbers
  sendFrom = getSendFrom(currentCommunicator->pivot, currentCommunicator->rank, size);
  //printf("%d: %05d:  My send from: %d with pivot: %d\n", rank, msg++, sendFrom, currentCommunicator->pivot);

  sendTo = currentlyEven ? currentCommunicator->rank + 1 : currentCommunicator->rank - 1;
  //printf("%d: %05d: dimension: %d I will send to: %d count %d\n", rank, msg++, dimension, sendTo, size - sendFrom);

  if (currentlyEven) {
    MPI_Send(list + sendFrom, size - sendFrom, MPI_INT, sendTo, 0, currentCommunicator->communicator);
    MPI_Recv(buffer, SIZE, MPI_INT, sendTo, 0, currentCommunicator->communicator, &status);
  } else {
    MPI_Recv(buffer, SIZE, MPI_INT, sendTo, 0, currentCommunicator->communicator, &status);
    MPI_Send(list + sendFrom, size - sendFrom, MPI_INT, sendTo, 0, currentCommunicator->communicator);
  }

  MPI_Get_count(&status, MPI_INT, &countReceived);
  memcpy((void *)(list + sendFrom), (void *)buffer, sizeof(int) * countReceived);

  size = sendFrom + countReceived;
  sizeAtEachStep[dimension] = size;
  exchangeTime += When() - startTime;
  //printList(list, size, msg++);

  // go to the next dimension
  step(dimension + 1, pivotStrategy);
}

// Gets the point in list from which to send and receive
// pivot - the value we pivot on
// myRank - my current rank (since we may be in the higher dimensions)
// mySize - my current size, since this may be happening after some swapping
int getSendFrom(int pivot, int myRank, int mySize) {
  int i;

  for (i = 0; i < mySize; ++i) {
    if (myRank % 2 == 0) {
      if (list[i] > pivot) break;
    } else {
      if (list[i] <= pivot) break;
    }
  }
  
  return i;
}

void genList() {
  int i;

  for (i = 0; i < size; ++i) {
    list[i] = rand() % MAX_VALUE;
  }
}

void printList(int *myList, int count, int line) {
  int i;

  fprintf(stderr, "%d, 1", rank);

  for (i=0; i < count; ++i) { 
    fprintf(stderr, ", %d", myList[i]); 
  }

  fprintf(stderr, "\n");
}

int randomValueInList(int *myList, int mySize) {
  return myList[rand() % mySize];
}

void buildCommunicators(int toDimension) {
  int previousDimension = toDimension - 1;
  Communicator *previousCommunicator;
  Communicator *currentCommunicator;
  int isEven;

  if (toDimension >= numDimensions) return;

  previousCommunicator = &communicators[previousDimension];
  currentCommunicator = &communicators[toDimension];
  isEven = previousCommunicator->rank % 2 == 0;

  MPI_Comm_split(previousCommunicator->communicator, isEven, previousCommunicator->rank, &currentCommunicator->communicator);
  MPI_Comm_rank(currentCommunicator->communicator, &currentCommunicator->rank);
  MPI_Comm_size(currentCommunicator->communicator, &currentCommunicator->communicatorSize);

  //printf("(world rank %d) dimension: %d, new rank: %d, previous rank %d\n", rank, toDimension, currentCommunicator->rank, previousCommunicator->rank);

  buildCommunicators(toDimension + 1);
}

int getMedian(int *arr, int size) {
  if (size % 2 == 0) {
    return( (arr[size / 2] + arr[size / 2 - 1]) / 2);
  } else {
    return arr[size / 2];
  }
}

void pivotFromMedianOfRootList(Communicator *comm) {
  double startTime;

  startTime = When();
  if (comm->rank == 0) {
    comm->pivot = getMedian(list, size);
  }
  compareTime += When() - startTime;

  startTime = When();
  MPI_Bcast(&comm->pivot, 1, MPI_INT, 0, comm->communicator);
  exchangeTime += When() - startTime;
}

void pivotFromMedianOfMedians(Communicator *comm) {
  int i;
  double startTime;

  startTime = When();
  medians[comm->rank] = getMedian(list, size);
  //printf("%d: My median is %d\n", rank, medians[comm->rank]);
  //printf("%d: comm size: %d\n", rank, comm->communicatorSize);
  for (i=0; i < comm->communicatorSize; ++i) {
    MPI_Bcast(&medians[i], 1, MPI_INT, i, comm->communicator);
  }

  qsort(medians, comm->communicatorSize, sizeof(int), ascending);
  comm->pivot = getMedian(medians, comm->communicatorSize);
  compareTime += When() - startTime;
}

void pivotFromRandomElementInRootList(Communicator *comm) {
  double startTime;

  startTime = When();
  if (comm->rank == 0) {
    comm->pivot = randomValueInList(list, size);
  }
  compareTime += When() - startTime;

  startTime = When();
  MPI_Bcast(&comm->pivot, 1, MPI_INT, 0, comm->communicator);
  exchangeTime += When() - startTime;
}

void randomPivot(Communicator *comm) {
  double startTime;

  startTime = When();
  if (comm->rank == 0) {
    comm->pivot = rand() % MAX_VALUE;
  }
  compareTime += When() - startTime;

  startTime = When();
  MPI_Bcast(&comm->pivot, 1, MPI_INT, 0, comm->communicator);
  exchangeTime += When() - startTime;
}

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
