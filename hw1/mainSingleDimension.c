#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>
#include <stdlib.h>
#include <stdbool.h>

#define HEIGHT 4096
#define WIDTH 4096
#define DELTA 0.1f
#define BOTTOM_BOUND ( HEIGHT - 1 ) * WIDTH
#define SIZE HEIGHT * WIDTH

bool gridNeedsAnotherUpdate(float *grid);
void initGrids();
float initialValueForCell(int i);
void printGrid(float *grid);

float grid1[HEIGHT * WIDTH];
float grid2[HEIGHT * WIDTH];
bool needsUpdateMap[WIDTH * HEIGHT];

/* Return the current time in seconds, using a double precision number. */
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  char* numThreads = getenv("OMP_NUM_THREADS");
  char* id = "single";

  double startTime = When();

  int iterationsCompleted = 0;

  // Grab the initial grids
  float *tMinusOne = grid1;
  float *t = grid2;

  bool weNeedAnotherRound = true;

  int i;

  initGrids();

  while (weNeedAnotherRound) {
    #pragma omp parallel for private(i)
      for (i = WIDTH; i < BOTTOM_BOUND; ++i) {
        if (needsUpdateMap[i]) {
          // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8

          t[i] = ( 
                          tMinusOne[i-WIDTH] +     // above
                          tMinusOne[i+WIDTH] +     // below
                          tMinusOne[i-1] +     // left
                          tMinusOne[i+1] +     // right
                          (4 * tMinusOne[i])   // this one
                        ) * 0.125f;
          
        }
      }

    ++iterationsCompleted;

    weNeedAnotherRound = gridNeedsAnotherUpdate(t);

    if (weNeedAnotherRound) {
      float *temp = t;
      t = tMinusOne;
      tMinusOne = temp;
    }
  }

  double endTime = When();

  printf("(%s) %s threads completed in %f seconds with %d iterations\n", id, numThreads, endTime-startTime, iterationsCompleted);

  return 0;
}

bool gridNeedsAnotherUpdate(float *grid) {
  int i;

  for (i = WIDTH; i < BOTTOM_BOUND - 1; ++i) {
    if (!needsUpdateMap[i]) continue;

      if ( fabs( 
               ( ( grid[i-WIDTH] + grid[i+WIDTH] + grid[i-1] + grid[i+1] ) * 0.25f )  - 
               grid[i]
             ) > DELTA ) return true;

  }

  return false; // These means that all neighbors were within allowable delta
}

void initGrids() {
  int i;

  for (i=0; i < SIZE; ++i) {
    grid1[i] = grid2[i] = initialValueForCell(i);
    needsUpdateMap[i] = grid1[i] == 50.0f;
  }
}

float initialValueForCell(int i) {
  int row = i / WIDTH;
  int col = i % HEIGHT;

  if (row == HEIGHT - 1 && col != 0 && col != HEIGHT - 1) {
    return 100.0f;
  } else if (row == 0 || col == 0 || col == HEIGHT - 1) {
    return 0.0f;
  } else if (row == 400 && (col >= 0 && col <= 330)) { // row 400 columns 0 through 330 are fixed at 100 degrees.
    return 100.0f;
  } else if (row == 200 && col == 500) { // A cell at row 200, column 500 also is fixed at 100 degrees.
    return 100.0f;
  } else {
    return 50.0f;
  }
}

void printGrid(float *grid) {
  int i;

  for (i=0; i < SIZE; ++i) {
    if (i % HEIGHT == 0) printf("\n");

    printf("%f ", grid[i]);
  } 

  printf("\n");
}
