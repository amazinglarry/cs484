#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>
#include <stdlib.h>
#include <stdbool.h>

#define HEIGHT 4096
#define WIDTH 4096
#define DELTA 0.1f

bool gridNeedsAnotherUpdate(float (* grid)[WIDTH]);
void initValueGrid(float (* grid)[WIDTH]);
void initNeedsUpdateGrid();
float initialValueForCell(int row, int col);
void printGrid(float (* grid)[WIDTH]);

float grid1[HEIGHT][WIDTH];
float grid2[HEIGHT][WIDTH];
bool needsUpdateMap[WIDTH][HEIGHT];

/* Return the current time in seconds, using a double precision number. */
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  char* numThreads = getenv("OMP_NUM_THREADS");
  char* id = "normal";

  double startTime = When();

  int iterationsCompleted = 0;

  // Grab the initial grids
  float (*tMinusOne)[WIDTH] = grid1;
  float (*t)[WIDTH] = grid2;

  bool weNeedAnotherRound = true;

  int row, col;

  initValueGrid(tMinusOne);
  initValueGrid(t);
  initNeedsUpdateGrid();

  while (weNeedAnotherRound) {
    #pragma omp parallel for private(row, col)
      for (row = 1; row < HEIGHT - 1; ++row) {
        for (col = 1; col < WIDTH - 1; ++col) {
          if (needsUpdateMap[row][col]) {

            // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8
            t[row][col] = ( 
                            tMinusOne[row-1][col] +     // above
                            tMinusOne[row+1][col] +     // below
                            tMinusOne[row][col-1] +     // left
                            tMinusOne[row][col+1] +     // right
                            (4 * tMinusOne[row][col])   // this one
                          ) * 0.125f;
          }
        }
      }

    ++iterationsCompleted;

    weNeedAnotherRound = gridNeedsAnotherUpdate(t);

    if (weNeedAnotherRound) {
      float (* temp)[WIDTH] = t;
      t = tMinusOne;
      tMinusOne = temp;
    }
  }

  double endTime = When();

  printf("(%s) %s threads completed in %f seconds with %d iterations\n", id, numThreads, endTime-startTime, iterationsCompleted);

  return 0;
}

bool gridNeedsAnotherUpdate(float (* grid)[WIDTH]) {
  int row, col;

  for (row = 1; row < HEIGHT - 1; ++row) {
    for (col = 1; col < WIDTH - 1; ++col) {
      if (!needsUpdateMap[row][col]) continue;

      if ( fabs( 
               ( ( grid[row-1][col] + grid[row+1][col] + grid[row][col-1] + grid[row][col+1] ) * 0.25f )  - 
               grid[row][col] 
             ) > DELTA )  return true;

    }
  }

  return false; // These means that all neighbors were within allowable delta
}

void initValueGrid(float (* grid)[WIDTH]) {
  int row, col;

  for (row=0; row < HEIGHT; ++row ) {
    for (col=0; col < WIDTH; ++col) {
      grid[row][col] = initialValueForCell(row, col);
    }
  }
}

void initNeedsUpdateGrid() {
  int row, col;

  for (row=0; row < HEIGHT; ++row ) {
    for (col=0; col < WIDTH; ++col) {
      needsUpdateMap[row][col] = initialValueForCell(row, col) == 50.0f;
    }
  }
}

float initialValueForCell(int row, int col) {
  if (row == HEIGHT - 1) {
    return 100.0f;
  } else if (row == 0 || col == 0 || col == HEIGHT - 1) {
    return 0.0f;
  } else if (row == 400 && (col >= 0 && col <= 330)) { // row 400 columns 0 through 330 are fixed at 100 degrees.
    return 100.0f;
  } else if (row == 200 && col == 500) { // A cell at row 200, column 500 also is fixed at 100 degrees.
    return 100.0f;
  } else {
    return 50.0f;
  }
}

void printGrid(float (* grid)[WIDTH]) {
  int row, col;

  for (row=0; row < HEIGHT; ++row) {
    for (col=0; col < WIDTH; ++col ) {
      printf("%f ", grid[row][col]);
    }

    printf("\n");
  } 

  printf("\n");
}

void printNeedsUpdateGrid(bool (* grid)[WIDTH], int width, int height) {
  int row, col;

  for (row=0; row < height; ++row) {
    for (col=0; col < width; ++col ) {
      printf("%d ", grid[row][col]);
    }

    printf("\n");
  } 
}
