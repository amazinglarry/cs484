# HW 1: Hotplate OpenMP fsl

## Overview of solution

My solution included 3 phases:

1. Initialization
2. Execution of a tick
3. Checking to see if another tick was needed

### Initialization

For initialization, I allocated 3 arrays of size 4096x4096.  I allocated them as multi-dimensional arrays.  2 were `float **`, and 1 was a `bool **`.  The `bool **` was a lookup to see if a given cell needed to be updated each tick.  I worked under the assumption that any cell with a fixed temperature did not require updating.  Both types of arrays were laid out in a row-major manner.

My initial initialization was all in serial.  Because it completed in less than a second, I didn't bother attempting to parallelize it until getting to the experimentation portion of the lab.

I decided what value to initialize each cell in the array to by using a rather unwieldy `if-else` statement, whose guards were based on the pre-defined values in the homework description.

### Execution of a tick

I used the 2 `float **` arrays in much the same way that a video game programmer uses double buffering.  On the first tick, array 1 represented time `t minus 1` and array 2 was where I wrote the new values for time `t`.  Upon determining that another tick was needed, I swapped the arrays' roles.  In this way, I didn't need to recreate the arrays for each tick in the simulation.

The actual computation code consisted of a nested `for` loop that iterated over rows on the outside, and then over the columns on the inside.  These loops did not start at 0, since updating the borders of the array never had to happen.  Before each recalculation, I did check to see if the corresponding cell in the `bool **` array refernced above indicated that the current cell required an update.

I used a number of temporary variables (e.g. setting `float above = tMinusOne[row-1][col]` so that the calculation for each tick would read more clearly than a single statement navigating the array would have shown.  I'm not generally familiar with `gcc`'s ability to optimize out such assignments, but if it doesn't, then there's a good chance that these additional indirections increased total computation time.

### Checking to see if another tick was needed.

After calculating updated values for a tick, I think checked to see if another tick was required. This involved another double `for` loop similar to the one used for calculating values.  I would check the current coordinates against by `bool **` array, because again, I did not consider the fixed-temperature cells when checking for the steady state.  This doulbe loop had the advantage that if any one had not reached steady state, I could skip checking the rest of the array.

### Performance of solution

Running this entirely serially took 68.2642 seconds on a 16 core machine (which might as well have been a 1 core machine because it wasn't threaded).

## Parallelizing the solution

My initial attempt at parallelizing my implementation was to simply use `#pragma omp parallel` and `#pragma omp for` around the outer loop in my tick update code.  Checking for completion required that all cells involved in the check have been computed, and while it certainly would have been possible to, for example, update the first 2 rows and then immediately start checking the first row for completion while row 3 was still being calculated, I opted for the conceptually simpler design of updating the whole grid before checking for completeness.  I also did not parallelize the check for completeness.

