#include <iostream>
#include <cmath>
#include <sys/time.h>
#include <omp.h>
#include <cstdlib>
using namespace std;

void clearGrid(float **grid, int height);
void clearNeedsUpdateGrid(bool **grid, int height);
bool gridNeedsAnotherUpdate(float **rowMajorGrid, float **colMajorGrid, bool **considerationGrid, int width, int height, float delta);
float** initValueGridRowMajor(int width, int height);
float** initValueGridColMajor(int width, int height);
bool** initNeedsUpdateGrid(int width, int height);
float initialValueForCell(int row, int col, int width, int height);
void printGrid(float **grid, int width, int height);
void printNeedsUpdateGrid(bool **grid, int width, int height);

/* Return the current time in seconds, using a double precision number. */
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  char* numThreads = getenv("OMP_NUM_THREADS");
  char* id = "both";

  double startTime = When();

  int iterationsCompleted = 0;

  // Grid dimensions
  int width = 4096;
  int height = 4096;

  // Params
  float completionDelta = 0.1f;

  // Grab the initial grids
  float **tMinusOneRowMajor = initValueGridRowMajor(width, height);
  float **tMinusOneColMajor = initValueGridColMajor(width, height);
  float **tRowMajor = initValueGridRowMajor(width, height); // TODO: Does this really need to be fully initialized?
  float **tColMajor = initValueGridColMajor(width, height); // TODO: Does this really need to be fully initialized?
  bool **needsUpdateMap = initNeedsUpdateGrid(width, height);

  bool weNeedAnotherRound = true;

  while (weNeedAnotherRound) {
    #pragma omp parallel 
    #pragma omp for
      for (int row = 1; row < height - 1; ++row) {
        for (int col = 1; col < width - 1; ++col) {
          if (needsUpdateMap[row][col]) {
            float above = tMinusOneColMajor[col][row-1];
            float below = tMinusOneColMajor[col][row+1];
            float left = tMinusOneRowMajor[row][col-1];
            float right = tMinusOneRowMajor[row][col+1];
            float previous = tMinusOneRowMajor[row][col];

            // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8
            float newVal = (above + below + left + right + (4 * previous)) / 8.0f;

            tRowMajor[row][col] = newVal;
            tColMajor[col][row] = newVal;
          }
        }
      }

    ++iterationsCompleted;

    weNeedAnotherRound = gridNeedsAnotherUpdate(tRowMajor, tColMajor, needsUpdateMap, width, height, completionDelta);

    if (weNeedAnotherRound) {
      float **temp = tRowMajor;
      tRowMajor = tMinusOneRowMajor;
      tMinusOneRowMajor = temp;

      temp = tColMajor;
      tColMajor = tMinusOneColMajor;
      tMinusOneColMajor = temp;
    }
  }

  clearGrid(tMinusOneRowMajor, height);
  clearGrid(tMinusOneColMajor, height);
  clearGrid(tRowMajor, height);
  clearGrid(tColMajor, height);
  clearNeedsUpdateGrid(needsUpdateMap, height);

  double endTime = When();

  cout << "(" << id << ")" << numThreads << " threads completed in " << endTime - startTime << " seconds with " << iterationsCompleted << " iterations." << endl;

  return 0;
}

bool gridNeedsAnotherUpdate(float **rowMajorGrid, float **colMajorGrid, bool **considerationGrid, int width, int height, float delta) {
  for (int row = 1; row < height - 1; ++row) {
    for (int col = 1; col < width - 1; ++col) {
      if (!considerationGrid[row][col]) continue;

      float current = rowMajorGrid[row][col];
      float upVal = colMajorGrid[col][row-1];
      float downVal = colMajorGrid[col][row+1];
      float leftVal = rowMajorGrid[row][col-1];
      float rightVal = rowMajorGrid[row][col+1];

      float avgNeighbors = (upVal + downVal + leftVal + rightVal) / 4.0f;

      if (abs(current - avgNeighbors) > delta) return true;
    }
  }

  return false; // These means that all neighbors were within allowable delta
}

// Caller is responsible for freeing the memory
float** initValueGridRowMajor(int width, int height) {
  float **grid = new float *[height];

  for (int row=0; row < height; ++row ) {
    grid[row] = new float[width];
 
    for (int col=0; col < width; ++col) {
      grid[row][col] = initialValueForCell(row, col, width, height);
    }
  }

  return grid;
}

// Caller is responsible for freeing the memory
float** initValueGridColMajor(int width, int height) {
  float **grid = new float *[width];

  for (int col=0; col < width; ++col) {
    grid[col] = new float[height];
 
    for (int row=0; row < height; ++row ) {
      grid[col][row] = initialValueForCell(row, col, width, height);
    }
  }

  return grid;
}

// Caller is responsible for freeing the memory
bool** initNeedsUpdateGrid(int width, int height) {
  bool **grid = new bool *[height];

  for (int row=0; row < height; ++row ) {
    grid[row] = new bool[width];
 
    for (int col=0; col < width; ++col) {
      grid[row][col] = initialValueForCell(row, col, width, height) == 50.0f;
    }
  }

  return grid;
}

void clearGrid(float** grid, int height) {
  for (int row=0; row < height; ++row) {
    delete [] grid[row]; 
  }

  delete [] grid;
}

void clearNeedsUpdateGrid(bool** grid, int height) {
  for (int row=0; row < height; ++row) {
    delete [] grid[row]; 
  }

  delete [] grid;
}

float initialValueForCell(int row, int col, int height, int width) {
  if (row == height - 1 && col != 0 && col != width - 1) {
    return 100.0f;
  } else if (row == 0 || col == 0 || col == width - 1) {
    return 0.0f;
  } else if (row == 400 && (col >= 0 && col <= 330)) { // row 400 columns 0 through 330 are fixed at 100 degrees.
    return 100.0f;
  } else if (row == 200 && col == 500) { // A cell at row 200, column 500 also is fixed at 100 degrees.
    return 100.0f;
  } else {
    return 50.0f;
  }
}

void printGridRowMajor(float **grid, int width, int height) {
  for (int row=0; row < height; ++row) {
    for (int col=0; col < width; ++col ) {
      cout << grid[row][col] << " ";
    }

    cout << endl;
  } 

  cout << endl;
}

void printGridColMajor(float **grid, int width, int height) {
  for (int col=0; col < width; ++col ) {
    for (int row=0; row < height; ++row) {
      cout << grid[col][row] << " ";
    }

    cout << endl;
  } 

  cout << endl;
}

void printNeedsUpdateGrid(bool **grid, int width, int height) {
  cout << width << endl;
  cout << height << endl;
  for (int row=0; row < height; ++row) {
    for (int col=0; col < width; ++col ) {
      cout << grid[row][col] << " ";
    }

    cout << endl;
  } 
}
