#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

#define DIMENSIONS 16384
#define HEIGHT DIMENSIONS
#define WIDTH DIMENSIONS
#define DELTA 0.1f
#define MAXTHREADS 24

void initValueGrids(long startRow, long endRow);
float initialValueForCell(int row, int col);
//void printGrid(float (* grid)[WIDTH]);

bool **needsUpdateMap;
bool weNeedAnotherRound = true;
int iterationsCompleted = 0;
int numThreads;
bool isOverList[MAXTHREADS];
float **t;
float **tMinusOne;
int countOver50[MAXTHREADS];
int totalOver50 = 0;

// Variables for synchronization
pthread_barrier_t barrier;

void *threadFunc(void *arg) {
  long threadId = (long)arg;
  long row, col;
  long startRow = HEIGHT / numThreads * threadId;
  long endRow = HEIGHT / numThreads * (threadId + 1) ;
  int i;

  printf("(%ld) %ld - %ld\n", (long)arg, startRow, endRow-1);

  // begin the loop
  while (weNeedAnotherRound) {
    countOver50[threadId] = 0;

    // DO THE TICK
    for (row = startRow; row < endRow - 1; ++row) {
      for (col = 1; col < WIDTH - 1; ++col) {
        if (needsUpdateMap[row][col]) {

          // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8
          t[row][col] = ( 
                          tMinusOne[row-1][col] +     // above
                          tMinusOne[row+1][col] +     // below
                          tMinusOne[row][col-1] +     // left
                          tMinusOne[row][col+1] +     // right
                          (4 * tMinusOne[row][col])   // this one
                        ) * 0.125f;

          if (t[row][col] > 50.0f) ++countOver50[threadId];
        }
      }
    }

    pthread_barrier_wait(&barrier);

    // SEE IF WE NEED ANOTHER TICK
    isOverList[threadId] = false;
    for (row = startRow; row < endRow - 1; ++row) {
      if (isOverList[threadId]) break;
      for (col = 1; col < WIDTH - 1; ++col) {
        if (isOverList[threadId]) break;
        if (needsUpdateMap[row][col]) {

          if (fabs( 
                   ( ( t[row-1][col] + t[row+1][col] + t[row][col-1] + t[row][col+1] ) * 0.25f )  - 
                   t[row][col] 
                 ) > DELTA) {
            isOverList[threadId] = true;
          }
        }
      }
    }

    if (threadId == 0) {
      ++iterationsCompleted;

      totalOver50 = 0;
      weNeedAnotherRound = false;
      for (i=0; i < numThreads; ++i) {
        weNeedAnotherRound = weNeedAnotherRound || isOverList[i];
        totalOver50 += countOver50[i];
      }

      if (weNeedAnotherRound) {
        float ** temp = t;
        t = tMinusOne;
        tMinusOne = temp;
      }

      printf("%d:%d\n", iterationsCompleted, totalOver50);
    }

    pthread_barrier_wait(&barrier);
  }
    // do the tick
    // check for if need update

  pthread_exit(NULL);
}

/* Return the current time in seconds, using a double precision number. */
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  double startTime = When();

  char* id = "pthread barrier";

  int i;

  // pthread stuff
  pthread_t threads[MAXTHREADS];
  pthread_attr_t attr;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  numThreads = atoi(getenv("OMP_NUM_THREADS"));
  pthread_barrier_init(&barrier, NULL, numThreads);

  // init memory
  initValueGrids(0, HEIGHT);

  for (i = 0; i < numThreads; ++i) {
    pthread_create(&threads[i], &attr, &threadFunc, (void *)(long)i);
  }

  for (i = 0; i < numThreads; ++i) {
    pthread_join(threads[i], NULL);
  }

  double endTime = When();

  printf("(%s) %d threads completed in %f seconds with %d iterations\n", id, numThreads, endTime-startTime, iterationsCompleted);

  return 0;
}

void initValueGrids(long startRow, long endRow) {
  long row, col;

  t = malloc(sizeof(float **) * HEIGHT);
  tMinusOne = malloc(sizeof(float **) * HEIGHT);
  needsUpdateMap = malloc(sizeof(bool **) * HEIGHT);

  for (row=0; row < HEIGHT; ++row ) {
    t[row] = malloc(sizeof(float *) * WIDTH);
    tMinusOne[row] = malloc(sizeof(float *) * WIDTH);
    needsUpdateMap[row] = malloc(sizeof(bool *) * WIDTH);
    for (col=0; col < WIDTH; ++col) {
      t[row][col] = tMinusOne[row][col] = initialValueForCell(row, col);
      needsUpdateMap[row][col] = t[row][col] == 50.0f;
    }
  }
  
  // Every 20 rows = 100
  for(row = 0; row < HEIGHT; row++) {
    if((row % 20) == 0) {
      for(col = 0; col < WIDTH; col++) {
        t[row][col] = 100.0f;
        tMinusOne[row][col] = 100.0f;
        needsUpdateMap[row][col] = false;
      }
    }
  }
  
  // Every 20 cols = 0
  for(col = 0; col < WIDTH; col++) {
    if((col % 20) == 0) {
      for(row = 0; row < HEIGHT; row++) {
        t[row][col] = tMinusOne[row][col] = 0.0f;
        needsUpdateMap[row][col] = false;
      }
    }
  }
}

float initialValueForCell(int row, int col) {
  if (row == HEIGHT - 1) {
    return 100.0f;
  } else if (row == 0 || col == 0 || col == HEIGHT - 1) {
    return 0.0f;
  } else if (row == 400 && (col >= 0 && col <= 330)) { // row 400 columns 0 through 330 are fixed at 100 degrees.
    return 100.0f;
  } else if (row == 200 && col == 500) { // A cell at row 200, column 500 also is fixed at 100 degrees.
    return 100.0f;
  } else {
    return 50.0f;
  }
}


void printGrid(float (* grid)[WIDTH]) {
  int row, col;

  for (row=0; row < HEIGHT; ++row) {
    for (col=0; col < WIDTH; ++col ) {
      printf("%f ", grid[row][col]);
    }

    printf("\n");
  } 

  printf("\n");
}

void printNeedsUpdateGrid(bool (* grid)[WIDTH], int width, int height) {
  int row, col;

  for (row=0; row < height; ++row) {
    for (col=0; col < width; ++col ) {
      printf("%d ", grid[row][col]);
    }

    printf("\n");
  } 
}
