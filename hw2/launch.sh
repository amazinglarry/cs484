#!/bin/bash

#SBATCH --time=0:30:00   # walltime
#SBATCH --ntasks=24   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=1024M   # memory per CPU core
#SBATCH -J "hw2"   # job name
#SBATCH --mail-user=juanpaco@gmail.com   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --qos=standby

# Compatibility variables for PBS. Delete if not needed.
export PBS_NODEFILE=`/fslapps/fslutils/generate_pbs_nodefile`
export PBS_JOBID=$SLURM_JOB_ID
export PBS_O_WORKDIR="$SLURM_SUBMIT_DIR"
export PBS_QUEUE=batch

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE
export OMP_NUM_THREADS=1
./hw2-pthread-barrier
./hw2-pthread-barrier-manual
./hw2-pthread-barrier-log

export OMP_NUM_THREADS=2
./hw2-pthread-barrier
./hw2-pthread-barrier-manual
./hw2-pthread-barrier-log

export OMP_NUM_THREADS=4
./hw2-pthread-barrier
./hw2-pthread-barrier-manual
./hw2-pthread-barrier-log

export OMP_NUM_THREADS=8
./hw2-pthread-barrier
./hw2-pthread-barrier-manual
./hw2-pthread-barrier-log

export OMP_NUM_THREADS=16
./hw2-pthread-barrier
./hw2-pthread-barrier-manual
./hw2-pthread-barrier-log
