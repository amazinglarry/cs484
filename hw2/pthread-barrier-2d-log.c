#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

#define DIMENSIONS 16384
#define HEIGHT DIMENSIONS
#define WIDTH DIMENSIONS
#define DELTA 0.1f
#define MAXTHREADS 24

void initValueGrids(long startRow, long endRow);
float initialValueForCell(int row, int col);
bool needsAnother(float** grid, int startRow, int endRow);

bool **needsUpdateMap;
bool weNeedAnotherRound = true;
int numThreads;
float **t;
float **tMinusOne;
float **temp;
int above50[MAXTHREADS];
bool needAnother[MAXTHREADS];
int iterationsCompleted = 0;

typedef struct {
  pthread_mutex_t countLock;
  pthread_cond_t okToProceedUp;
  pthread_cond_t okToProceedDown;
  int count;
} EthoLogBarrierNode; 

typedef struct {
  pthread_mutex_t countLock;
  int countInLock;
  EthoLogBarrierNode barrier[MAXTHREADS];
} EthoLogBarrier;

void ethoInitBarrier(EthoLogBarrier *b) {
  int i;

  for (i=0; i<MAXTHREADS; ++i) {
    b->barrier[i].count = 0;
    pthread_mutex_init(&(b->barrier[i].countLock), NULL);
    pthread_cond_init(&(b->barrier[i].okToProceedUp), NULL);
    pthread_cond_init(&(b->barrier[i].okToProceedDown), NULL);
  }
  b->countInLock = 0;
  pthread_mutex_init(&b->countLock, NULL);
}

bool needsAnother(float** grid, int startRow, int endRow) {
  int row, col;

  for (row = startRow; row < endRow - 1; ++row) {
    for (col = 1; col < WIDTH - 1; ++col) {
      if (needsUpdateMap[row][col]) {

        if (fabs( 
                 ( ( t[row-1][col] + t[row+1][col] + t[row][col-1] + t[row][col+1] ) * 0.25f )  - 
                 t[row][col] 
               ) > DELTA) {
          return true;
        }
      }
    }
  }

  return false;
}

void ethoLogBarrier(EthoLogBarrier *b, int threadId) {
  int i, prepareForNextRoundIndex, base, index;
  float **tmp;
  i = 2;
  base = 0;

  //printf("(%d) Entering barrier\n", threadId);
  if (numThreads == 1) return;

  pthread_mutex_lock(&b->countLock);
  b->countInLock++;
  //printf("(%d) updated count to %d\n", threadId, b->countInLock);
  if (b->countInLock == numThreads) {
   // printf("(%d) do prep for next round\n", threadId);
    // TODO: Get ready for the next round
  }
  pthread_mutex_unlock(&b->countLock);

  do {
    index = base + threadId / i;
    //printf("(%d) index is %d\n", threadId, index);

    pthread_mutex_lock(&b->barrier[index].countLock);
    b->barrier[index].count++;
    //printf("(%d) node count is %d\n", threadId, b->barrier[index].count);
    if (threadId % i == 0) {
      // We're the zeroth in a pair, so we need to wait for the 1th in the pair
      //printf("(%d) We're the zeroth in a pair, so we need to wait for the 1th in the pair\n", threadId);
      while (b->barrier[index].count < 2) pthread_cond_wait(&b->barrier[index].okToProceedUp, &b->barrier[index].countLock);
      pthread_mutex_unlock(&b->barrier[index].countLock);
      //printf("(%d) 1th arrived, so we're good to go\n", threadId);
    } else {
      // We're the 1th.  So we'll increment the count and check to see if we should signal the 0th. Then we'll wait until we can proceed down again
      //printf("(%d) We're the 1th and we updated the count to %d\n", threadId, b->barrier[index].count);
      if (b->barrier[index].count == 2) { // the 0th made it here first
        //printf("(%d) signal the 0th\n", threadId);
        pthread_cond_signal(&b->barrier[index].okToProceedUp);
      }

      //printf("(%d) wait to go back down\n", threadId);
      while (pthread_cond_wait(&b->barrier[index].okToProceedDown, &b->barrier[index].countLock) != 0);
      //printf("(%d) done with wait to go back down\n", threadId);

      pthread_mutex_unlock(&b->barrier[index].countLock);
      break;
    }

    // Only the 0th in the pair makes it this far
    base = base + numThreads/i;
    i = i * 2;
  } while ( i <= numThreads);

  // Start walking back down and signaling the pairs
  i = i / 2;

  for (; i > 1; i = i / 2) {
    base = base - numThreads / i;
    index = base + threadId / i;
    pthread_mutex_lock(&b->barrier[index].countLock);
    b->barrier[index].count = 0;
    pthread_cond_signal(&b->barrier[index].okToProceedDown);
    pthread_mutex_unlock(&b->barrier[index].countLock);
  }

  pthread_mutex_lock(&b->countLock);
  b->countInLock--;
  pthread_mutex_unlock(&b->countLock);
  //printf("(%d) leaving barrier\n", threadId);
}

// Variables for synchronization
EthoLogBarrier barrier;

void *threadFunc(void *arg) {
  int threadId = (long)arg;
  int row, col;
  int startRow = HEIGHT / numThreads * threadId;
  int endRow = HEIGHT / numThreads * (threadId + 1) ;
  int i, totalOver50; // Only thread 0 uses this

  printf("(%d) %d - %d\n", threadId, startRow, endRow-1);

  // begin the loop
  while (weNeedAnotherRound) {
    // Init this thread's locals for this tick
    above50[threadId] = 0;
    needAnother[threadId] = false;

    // DO THE TICK
    for (row = startRow; row < endRow - 1; ++row) {
      for (col = 1; col < WIDTH - 1; ++col) {
        if (needsUpdateMap[row][col]) {

          // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8
          t[row][col] = ( 
                          tMinusOne[row-1][col] +     // above
                          tMinusOne[row+1][col] +     // below
                          tMinusOne[row][col-1] +     // left
                          tMinusOne[row][col+1] +     // right
                          (4 * tMinusOne[row][col])   // this one
                        ) * 0.125f;

          if (t[row][col] > 50.0f) ++above50[threadId];
        }
      }
    }

    ethoLogBarrier(&barrier, threadId);

    needAnother[threadId] = needsAnother(t, startRow, endRow);

    ethoLogBarrier(&barrier, threadId);

    if (threadId == 0) {
      weNeedAnotherRound = needAnother[0];
      totalOver50 = above50[0];
      for (i = 1; i < numThreads; ++i) {
        totalOver50 += above50[i];
        weNeedAnotherRound = weNeedAnotherRound || needAnother[i];
      }

      ++iterationsCompleted;

      printf("%d:%d\n", iterationsCompleted, totalOver50);
      temp = t;
      t = tMinusOne;
      tMinusOne = temp;
    }

    ethoLogBarrier(&barrier, threadId);

    //afterRound(&barrier);
  }
    // do the tick
    // check for if need update

  printf("(%d) exiting\n", threadId);
  pthread_exit(NULL);
}

/* Return the current time in seconds, using a double precision number. */
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  double startTime = When();

  char* id = "log barrier";

  int i;

  // pthread stuff
  pthread_t threads[MAXTHREADS];
  pthread_attr_t attr;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  numThreads = atoi(getenv("OMP_NUM_THREADS"));
  ethoInitBarrier(&barrier);

  // init memory
  initValueGrids(0, HEIGHT);

  for (i = 0; i < numThreads; ++i) {
    printf("Spawning thread %d\n", i);
    pthread_create(&threads[i], &attr, &threadFunc, (void *)(long)i);
  }

  for (i = 0; i < numThreads; ++i) {
    pthread_join(threads[i], NULL);
  }

  double endTime = When();

  printf("(%s) %d threads completed in %f seconds with %d iterations\n", id, numThreads, endTime-startTime, 0);//barrier.iterations);

  return 0;
}

void initValueGrids(long startRow, long endRow) {
  long row, col;

  t = malloc(sizeof(float **) * HEIGHT);
  tMinusOne = malloc(sizeof(float **) * HEIGHT);
  needsUpdateMap = malloc(sizeof(bool **) * HEIGHT);

  for (row=0; row < HEIGHT; ++row ) {
    t[row] = malloc(sizeof(float *) * WIDTH);
    tMinusOne[row] = malloc(sizeof(float *) * WIDTH);
    needsUpdateMap[row] = malloc(sizeof(bool *) * WIDTH);
    for (col=0; col < WIDTH; ++col) {
      t[row][col] = tMinusOne[row][col] = initialValueForCell(row, col);
      needsUpdateMap[row][col] = t[row][col] == 50.0f;
    }
  }
  
  // Every 20 rows = 100
  for(row = 0; row < HEIGHT; row++) {
    if((row % 20) == 0) {
      for(col = 0; col < WIDTH; col++) {
        t[row][col] = 100.0f;
        tMinusOne[row][col] = 100.0f;
        needsUpdateMap[row][col] = false;
      }
    }
  }
  
  // Every 20 cols = 0
  for(col = 0; col < WIDTH; col++) {
    if((col % 20) == 0) {
      for(row = 0; row < HEIGHT; row++) {
        t[row][col] = tMinusOne[row][col] = 0.0f;
        needsUpdateMap[row][col] = false;
      }
    }
  }
}

float initialValueForCell(int row, int col) {
  if (row == HEIGHT - 1) {
    return 100.0f;
  } else if (row == 0 || col == 0 || col == HEIGHT - 1) {
    return 0.0f;
  } else if (row == 400 && (col >= 0 && col <= 330)) { // row 400 columns 0 through 330 are fixed at 100 degrees.
    return 100.0f;
  } else if (row == 200 && col == 500) { // A cell at row 200, column 500 also is fixed at 100 degrees.
    return 100.0f;
  } else {
    return 50.0f;
  }
}


void printGrid(float (* grid)[WIDTH]) {
  int row, col;

  for (row=0; row < HEIGHT; ++row) {
    for (col=0; col < WIDTH; ++col ) {
      printf("%f ", grid[row][col]);
    }

    printf("\n");
  } 

  printf("\n");
}

void printNeedsUpdateGrid(bool (* grid)[WIDTH], int width, int height) {
  int row, col;

  for (row=0; row < height; ++row) {
    for (col=0; col < width; ++col ) {
      printf("%d ", grid[row][col]);
    }

    printf("\n");
  } 
}
