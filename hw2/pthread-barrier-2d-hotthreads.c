#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

#define DIMENSIONS 16384
#define HEIGHT DIMENSIONS
#define WIDTH DIMENSIONS
#define DELTA 0.1f
#define MAXTHREADS 24

void initValueGrids(long startRow, long endRow);
float initialValueForCell(int row, int col);
bool needsAnother(float** grid, int startRow, int endRow);

bool **needsUpdateMap;
bool weNeedAnotherRound = true;
int numThreads;
float **t;
float **tMinusOne;
float **temp;
int above50[MAXTHREADS];
bool needAnother[MAXTHREADS];
int iterationsCompleted = 0;
pthread_mutex_t countLock;
int countInLock = 0;
bool volatile * waiting;

bool needsAnother(float** grid, int startRow, int endRow) {
  int row, col;

  for (row = startRow; row < endRow - 1; ++row) {
    for (col = 1; col < WIDTH - 1; ++col) {
      if (needsUpdateMap[row][col]) {

        if (fabs( 
                 ( ( t[row-1][col] + t[row+1][col] + t[row][col-1] + t[row][col+1] ) * 0.25f )  - 
                 t[row][col] 
               ) > DELTA) {
          return true;
        }
      }
    }
  }

  return false;
}

void hotThreadBarrier(int threadId) {
  int i;

  waiting[threadId] = true;

  pthread_mutex_lock(&countLock); 
  countInLock++;

  if (countInLock < numThreads) {
    pthread_mutex_unlock(&countLock);
    while (waiting[threadId]);
    //wait here
  } else {
    pthread_mutex_unlock(&countLock);
    countInLock = 0;
    for (i=0; i<numThreads; ++i) {
      waiting[i] = false;
    }
  }
}

void hotThreadBarrierAtEnd(int threadId) {
  int i;
  int totalOver50;

  if (numThreads == 1) return;

  waiting[threadId] = true;

  pthread_mutex_lock(&countLock); 
  countInLock++;

  if (countInLock < numThreads) {
    pthread_mutex_unlock(&countLock);
    while (waiting[threadId]);
  } else {
    pthread_mutex_unlock(&countLock);

    countInLock = 0;
    weNeedAnotherRound = false;
    totalOver50 = 0;
    for (i = 0; i < numThreads; ++i) {
      totalOver50 += above50[i];
      weNeedAnotherRound = weNeedAnotherRound || needAnother[i];
    }

    ++iterationsCompleted;

    printf("%d:%d\n", iterationsCompleted, totalOver50);
    temp = t;
    t = tMinusOne;
    tMinusOne = temp;

    for (i=0; i<numThreads; ++i) {
      waiting[i] = false;
    }
  }
}

void *threadFunc(void *arg) {
  int threadId = (long)arg;
  int row, col;
  int startRow = HEIGHT / numThreads * threadId;
  int endRow = HEIGHT / numThreads * (threadId + 1) ;
  int i, totalOver50; // Only thread 0 uses this

  printf("(%d) %d - %d\n", threadId, startRow, endRow-1);

  // begin the loop
  while (weNeedAnotherRound) {
    // Init this thread's locals for this tick
    above50[threadId] = 0;
    needAnother[threadId] = false;

    // DO THE TICK
    for (row = startRow; row < endRow - 1; ++row) {
      for (col = 1; col < WIDTH - 1; ++col) {
        if (needsUpdateMap[row][col]) {

          // FORMULA FROM HW DESCRIPTION: xi,j(t) = (xi+1,j(t-1) + xi-1,j(t-1) + xi,j+1(t-1) + xi,j-1(t-1) + 4 * xi,j(t-1))/8
          t[row][col] = ( 
                          tMinusOne[row-1][col] +     // above
                          tMinusOne[row+1][col] +     // below
                          tMinusOne[row][col-1] +     // left
                          tMinusOne[row][col+1] +     // right
                          (4 * tMinusOne[row][col])   // this one
                        ) * 0.125f;

          if (t[row][col] > 50.0f) ++above50[threadId];
        }
      }
    }

    hotThreadBarrier(threadId);

    needAnother[threadId] = needsAnother(t, startRow, endRow);
    
    hotThreadBarrierAtEnd(threadId);
  }
    // do the tick
    // check for if need update

  printf("(%d) exiting\n", threadId);
  pthread_exit(NULL);
}

/* Return the current time in seconds, using a double precision number. */
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {
  double startTime = When();

  char* id = "log barrier";

  int i;

  // pthread stuff
  pthread_t threads[MAXTHREADS];
  pthread_attr_t attr;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  numThreads = atoi(getenv("OMP_NUM_THREADS"));

  // init memory
  initValueGrids(0, HEIGHT);

  waiting = malloc(sizeof(bool) * MAXTHREADS);

  for (i = 0; i < numThreads; ++i) {
    waiting[i] = false;
    printf("Spawning thread %d\n", i);
    pthread_create(&threads[i], &attr, &threadFunc, (void *)(long)i);
  }

  for (i = 0; i < numThreads; ++i) {
    pthread_join(threads[i], NULL);
  }

  double endTime = When();

  printf("(%s) %d threads completed in %f seconds with %d iterations\n", id, numThreads, endTime-startTime, iterationsCompleted);

  return 0;
}

void initValueGrids(long startRow, long endRow) {
  long row, col;

  t = malloc(sizeof(float **) * HEIGHT);
  tMinusOne = malloc(sizeof(float **) * HEIGHT);
  needsUpdateMap = malloc(sizeof(bool **) * HEIGHT);

  for (row=0; row < HEIGHT; ++row ) {
    t[row] = malloc(sizeof(float *) * WIDTH);
    tMinusOne[row] = malloc(sizeof(float *) * WIDTH);
    needsUpdateMap[row] = malloc(sizeof(bool *) * WIDTH);
    for (col=0; col < WIDTH; ++col) {
      t[row][col] = tMinusOne[row][col] = initialValueForCell(row, col);
      needsUpdateMap[row][col] = t[row][col] == 50.0f;
    }
  }
  
  // Every 20 rows = 100
  for(row = 0; row < HEIGHT; row++) {
    if((row % 20) == 0) {
      for(col = 0; col < WIDTH; col++) {
        t[row][col] = 100.0f;
        tMinusOne[row][col] = 100.0f;
        needsUpdateMap[row][col] = false;
      }
    }
  }
  
  // Every 20 cols = 0
  for(col = 0; col < WIDTH; col++) {
    if((col % 20) == 0) {
      for(row = 0; row < HEIGHT; row++) {
        t[row][col] = tMinusOne[row][col] = 0.0f;
        needsUpdateMap[row][col] = false;
      }
    }
  }
}

float initialValueForCell(int row, int col) {
  if (row == HEIGHT - 1) {
    return 100.0f;
  } else if (row == 0 || col == 0 || col == HEIGHT - 1) {
    return 0.0f;
  } else if (row == 400 && (col >= 0 && col <= 330)) { // row 400 columns 0 through 330 are fixed at 100 degrees.
    return 100.0f;
  } else if (row == 200 && col == 500) { // A cell at row 200, column 500 also is fixed at 100 degrees.
    return 100.0f;
  } else {
    return 50.0f;
  }
}


void printGrid(float (* grid)[WIDTH]) {
  int row, col;

  for (row=0; row < HEIGHT; ++row) {
    for (col=0; col < WIDTH; ++col ) {
      printf("%f ", grid[row][col]);
    }

    printf("\n");
  } 

  printf("\n");
}

void printNeedsUpdateGrid(bool (* grid)[WIDTH], int width, int height) {
  int row, col;

  for (row=0; row < height; ++row) {
    for (col=0; col < width; ++col ) {
      printf("%d ", grid[row][col]);
    }

    printf("\n");
  } 
}
