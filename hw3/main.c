#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <math.h>

#define VECSIZE 131072
//#define VECSIZE 65536
//#define VECSIZE 32768
//#define VECSIZE 16384
//#define VECSIZE 12288
//#define VECSIZE 8192
//#define VECSIZE 2
//#define VECSIZE 8
//#define ITERATIONS 10000
#define ITERATIONS 100

typedef struct {
    double val;
    int   rank;
} TransmissionValue;

void broadcast(int numDimensions, int rank, TransmissionValue *value) {
  int notparticipating = pow(2,numDimensions-1)-1;
  int bitmask = pow(2,numDimensions-1);
  int curdim;
  MPI_Status status;

  for(curdim = 0; curdim < numDimensions; curdim++) {
    if ((rank & notparticipating) == 0) {
      if ((rank & bitmask) == 0) {
        int msg_dest = rank ^ bitmask;
        //printf("(%d) broadcast send to %d (%f)\n", rank, msg_dest, value[0].val);
        MPI_Send(value, VECSIZE, MPI_DOUBLE_INT, msg_dest, 0, MPI_COMM_WORLD);
      } else {
        int msg_src = rank ^ bitmask;
        //printf("(%d) broadcast receive from %d\n", rank, msg_src);
        MPI_Recv(value, VECSIZE, MPI_DOUBLE_INT, msg_src, 0, MPI_COMM_WORLD, &status);
      }
    }

    notparticipating >>= 1;
    bitmask >>=1;
  }
}

void collapseMax(TransmissionValue *from, TransmissionValue *to) {
  int i = 0;

  for (i=0; i < VECSIZE; ++i) {
//    printf("(%d) collapseMax: from: %f to: %f\n", i, from[i].val, to[i].val);
    if (from[i].val > to[i].val) {
  //    printf("I will collapse\n");
      to[i].val = from[i].val;
      to[i].rank = from[i].rank;
    }
  }
}

float reduce(int numDimensions, int rank, TransmissionValue *send, TransmissionValue *receive) {
  int notparticipating = 0;
  int bitmask = 1;
  int curdim;
  MPI_Status status;

  for(curdim = 0; curdim < numDimensions; curdim++) {
    if ((rank & notparticipating) == 0) {
      if ((rank & bitmask) != 0) {
        int msg_dest = rank ^ bitmask;
 //       printf("(%d) reduce send to %d (%f)\n", rank, msg_dest, send[0].val);
        MPI_Send(send, VECSIZE, MPI_DOUBLE_INT, msg_dest, 0, MPI_COMM_WORLD);
      } else {
        int msg_src = rank ^ bitmask;
   //     printf("(%d) reduce receive from %d\n", rank, msg_src);
        MPI_Recv(receive, VECSIZE, MPI_DOUBLE_INT, msg_src, 0, MPI_COMM_WORLD, &status);
        collapseMax(receive, send);
      }
    }

    notparticipating = notparticipating ^ bitmask;
    bitmask <<=1;
  }
}

double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main(int argc, char *argv[]) {
  int iproc, nproc,i, iter;
  char host[255];
  MPI_Status status;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &iproc);

  int numDimensions = (int)log2(nproc);
  //printf("(%d) numDimensions: %d\n", iproc, numDimensions);

  gethostname(host,253);
  //printf("I am proc %d of %d running on %s\n", iproc, nproc,host);
  // each process has an array of VECSIZE double: ain[VECSIZE]
  double ain[VECSIZE], aout[VECSIZE];
  int ind[VECSIZE];
  TransmissionValue in[VECSIZE], out[VECSIZE];
  int myrank, root = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  // Start time here
  srand(myrank+5);
  double start = When();
  for(iter = 0; iter < ITERATIONS; iter++) {
    for(i = 0; i < VECSIZE; i++) {
      ain[i] = rand();
 //     printf("init proc %d [%d]=%f\n",myrank,i,ain[i]);
    }
    for (i=0; i<VECSIZE; ++i) {
      in[i].val = ain[i];
      in[i].rank = myrank;
    }
    reduce(numDimensions, iproc, in, out);
    // At this point, the answer resides on process root
    if (myrank == root) {
        /* read ranks out
         */
        for (i=0; i<VECSIZE; ++i) {
            //printf("root out[%d] = %f from %d\n",i,out[i].val,out[i].rank);
   //         printf("root in[%d] = %f from %d\n",i,in[i].val,in[i].rank);
            aout[i] = out[i].val;
            ind[i] = out[i].rank;
        }
    }
    // Now broadcast this max vector to everyone else.
    broadcast(numDimensions, iproc, in);
    for(i = 0; i < VECSIZE; i++) {
      //printf("final proc %d [%d]=%f from %d\n",myrank,i,out[i].val,out[i].rank);
    }
  }
  MPI_Finalize();
  double end = When();
  if(myrank == root) {
    printf("Time %f\n",end-start);
  }

  return 0;
}

